#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

int
main(int argc, char *argv[])
{
    int   pfds[2];
    int   result;
    pid_t pid;

    /* Create pipe */
    result = pipe(pfds);
    if (result < 0) {
    	perror("pipe");
    	return (EXIT_FAILURE);
    }

    /* Fork */
    pid = fork();
    switch (pid) {
    	/* Error */
	case -1:
	    perror("fork");
	    return (EXIT_FAILURE);
	
	/* Child */
	case 0:
	    /* Close stdin */
	    close(0);

	    /* Duplicate pipe read-end into stdin */
	    dup(pfds[0]);

	    /* We don't need this */
	    close(pfds[1]);

	    /* Execute wc -l */
	    result = execlp("wc", "wc", "-l", NULL);
	    if (result < 0) {
	    	perror("execlp");
	    }

	    _exit(EXIT_FAILURE);
	    break;

	/* Parent */
	default:
	    /* Close stdout */
	    close(1);

	    /* Duplicate pipe write-end into stdin */
	    dup(pfds[1]);
	    
	    /* We don't need this */
	    close(pfds[0]);

	    /* Execute ls -la */
	    execlp("ls", "ls", "-la", NULL);
	    if (result < 0) {
	    	perror("execlp");
	    }

	    _exit(EXIT_FAILURE);
	    break;
    }

    return (EXIT_SUCCESS);
}
