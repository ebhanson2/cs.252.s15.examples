/* socket.h: simple socket library */

#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <netdb.h>

int socket_resolve(const char *host, const char *port, struct addrinfo **hostinfo);
int socket_dial(const char *host, const char *port);
int socket_listen(const char *port);

#endif
