/* echo_server.c: simple TCP echo server (fork) */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    /* Parse command line options */
    if (argc != 2) {
	fprintf(stderr, "usage: %s port\n", argv[0]);
	return EXIT_FAILURE;
    }

    char *port = argv[1];

    /* Lookup server address information */
    struct addrinfo *results;
    struct addrinfo  hints;
    int    server_fd = -1;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;	/* Use either IPv4 (AF_INET) or IPv6 (AF_INET6) */
    hints.ai_socktype = SOCK_STREAM;	/* Use TCP  */
    hints.ai_flags    = AI_PASSIVE;	/* Use all interfaces */

    if (getaddrinfo(NULL, port, &hints, &results) < 0) {
    	fprintf(stderr, "Could not look up on port %s: %s\n", port, strerror(errno));
    	return EXIT_FAILURE;
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	char ip[INET6_ADDRSTRLEN];

	/* Translate IP address to string */
	if (p->ai_family == AF_INET) {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr), ip, sizeof(ip));
	} else {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in6 *)p->ai_addr)->sin6_addr), ip, sizeof(ip));
	}

	/* Allocate socket */
	if ((server_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Bind socket */
	if (bind(server_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to bind to %s:%s: %s\n", ip, port, strerror(errno));
	    close(server_fd);
	    continue;
	}

	/* Listen on socket */
	if (listen(server_fd, SOMAXCONN) < 0) {
	    fprintf(stderr, "Unable to listen on %s:%s: %s\n", ip, port, strerror(errno));
	    close(server_fd);
	    continue;
	}

	/* Successful setup */
	fprintf(stderr, "Listening on %s:%s\n", ip, port);
	break;
    }
    freeaddrinfo(results);

    /* Ignore children */
    signal(SIGCHLD, SIG_IGN);

    /* Accept and handle incoming connections */
    while (true) {
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	int client_fd;
	char client_host[NI_MAXHOST], client_port[NI_MAXSERV];
	FILE *client_file;
	char buffer[BUFSIZ];
	pid_t pid;

	/* Accept a client */
	client_addr_len = sizeof(client_addr);
	client_fd       = accept(server_fd, &client_addr, &client_addr_len);
	if (client_fd < 0) {
	    fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
	    continue;
	}

	/* Fork off child to handle client connection */
	pid = fork();
	if (pid < 0) {
	    fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
	    continue;
	}

	/* Child Process */
	if (pid == 0) {
	    int exit_status = EXIT_SUCCESS;

	    /* Lookup client information */
	    if (getnameinfo(&client_addr, client_addr_len, client_host, sizeof(client_host), client_port, sizeof(client_port), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
		fprintf(stderr, "Unable to lookup client: %s\n", strerror(errno));
		goto client_failure;
	    }

	    fprintf(stderr, "%s connected on %s\n", client_host, client_port);

	    /* Open socket stream */
	    client_file = fdopen(client_fd, "r+");
	    if (client_file < 0) {
		fprintf(stderr, "Unable to open file descriptor: %s\n", strerror(errno));
		goto client_failure;
	    }

	    /* Read from client, log to stdout, write to client */
	    while (true) {
		/* Read from client */
		if (fgets(buffer, BUFSIZ, client_file) == NULL) {
		    break;
		}

		/* Log to stdout */
		printf("%s:%s says %s", client_host, client_port, buffer);

		/* Echo to client */
		if (fputs(buffer, client_file) == EOF) {
		    break;
		}
	    }

	    fprintf(stderr, "%s:%s disconnected\n", client_host, client_port);
	    goto client_cleanup;

client_failure:
	    exit_status = EXIT_FAILURE;

client_cleanup:
	    close(client_fd);
	    return exit_status;
	
	/* Parent Process */
	} else {
	    /* Close client */
	    close(client_fd);
	}

	close(client_fd);
    }

    return EXIT_SUCCESS;
}
