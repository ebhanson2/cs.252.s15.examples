/* socket_echo_client.c: simple TCP echo client */

#include "socket.h"

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[])
{
    char *host;
    char *port;
    int   socket_fd;
    FILE *socket_file;
    char  buffer[BUFSIZ];

    /* Parse command line options */
    if (argc != 3) {
	fprintf(stderr, "usage: %s host port\n", argv[0]);
	return EXIT_FAILURE;
    }

    host = argv[1];
    port = argv[2];

    /* Dial server */
    socket_fd = socket_dial(host, port);
    if (socket_fd < 0) {
    	fprintf(stderr, "Could not dial %s:%s: %s\n", host, port, strerror(errno));
	return EXIT_FAILURE;
    }

    /* Open socket file from fd */
    socket_file = fdopen(socket_fd, "r+");
    if (socket_file == NULL) {
    	fprintf(stderr, "Could not open socket as FILE: %s\n", strerror(errno));
	return EXIT_FAILURE;
    }

    /* Send stdin to server and write results to stdout */
    while (true) {
	printf("> "); fflush(stdout);

	/* Read from stdin, Write to to server, Read from server */
	if (fgets(buffer, BUFSIZ, stdin) == NULL ||
	    fputs(buffer, socket_file) == EOF    ||
	    fgets(buffer, BUFSIZ, socket_file) == NULL) {
	    break;
	}

	/* Write to stdout */
	fputs(buffer, stdout);
    }

    fclose(socket_file);
    return EXIT_SUCCESS;
}
