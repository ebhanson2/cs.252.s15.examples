#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define FLOAT_BYTES sizeof(float)

int
main(int argc, char *argv[])
{
    union {
    	float	number;
    	uint8_t bytes[FLOAT_BYTES];
    } data;

    for (int i = 1; i < argc; i++) {
	data.number = strtof(argv[i], NULL);

	for (int c = 0; c < FLOAT_BYTES; c++) {
	    printf("%d: %02x\n", c, data.bytes[c]);
	}

	putchar('\n');
    }

    return EXIT_SUCCESS;
}
