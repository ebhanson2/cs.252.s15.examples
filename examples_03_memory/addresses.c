#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int    i = 321;
    char   c = 'p';
    float  f = 3.14;
    double d = 3.14;

    printf("i = %4d (%lu bytes) is located at 0x%lx (%lu bytes)\n", i, sizeof(i), (unsigned long)&i, sizeof(&i));
    printf("c = %4c (%lu bytes) is located at 0x%lx (%lu bytes)\n", c, sizeof(c), (unsigned long)&c, sizeof(&c));
    printf("f = %3.2f (%lu bytes) is located at 0x%lx (%lu bytes)\n", f, sizeof(f), (unsigned long)&f, sizeof(&f));
    printf("d = %3.2lf (%lu bytes) is located at 0x%lx (%lu bytes)\n", d, sizeof(d), (unsigned long)&d, sizeof(&d));

    return EXIT_SUCCESS;
}
