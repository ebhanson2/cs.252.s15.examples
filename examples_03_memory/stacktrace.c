#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int
twice(int x)
{
    int result = x + x;
    return result;
}

int
main(int argc, char *argv[])
{
    twice(1);
    twice(4);
    return EXIT_SUCCESS;
}
