/* socket_echo_server.c: simple TCP echo server (thread) */

#include "socket.h"

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

struct echo_client {
    int   fd;
    FILE *file;
    char  host[NI_MAXHOST];
    char  port[NI_MAXSERV];
};

int   accept_client(int server_fd, struct echo_client *client);
void* handle_client(void *arg);

int
main(int argc, char *argv[])
{
    char *port;
    int   server_fd;

    /* Parse command line options */
    if (argc != 2) {
	fprintf(stderr, "usage: %s port\n", argv[0]);
	return EXIT_FAILURE;
    }

    port = argv[1];

    /* Open server socket */
    server_fd = socket_listen(port);
    if (server_fd < 0) {
    	fprintf(stderr, "Unable to open socket on port %s: %s\n", port, strerror(errno));
    	return EXIT_FAILURE;
    }

    /* Accept and handle incoming connections */
    while (true) {
    	struct echo_client *client;
    	pthread_t thread;

	/* Allocate client struct */
	client = calloc(1, sizeof(struct echo_client));
	if (client == NULL) {
	    perror("calloc");
	    return EXIT_FAILURE;
	}

	/* Accept client */
    	if (accept_client(server_fd, client) < 0) {
    	    fprintf(stderr, "Could not accept client: %s\n", strerror(errno));
    	    continue;
	}

	pthread_create(&thread, NULL, handle_client, client);
	pthread_detach(thread);
    }

    return EXIT_SUCCESS;
}

int
accept_client(int server_fd, struct echo_client *client)
{
    struct sockaddr client_addr;
    socklen_t       client_size;

    /* Accept a client */
    client_size = sizeof(client_addr);
    client->fd  = accept(server_fd, &client_addr, &client_size);
    if (client->fd < 0) {
    	return -1;
    }

    /* Lookup client information */
    if (getnameinfo(&client_addr, client_size, client->host, NI_MAXHOST, client->port, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
    	close(client->fd);
    	return -1;
    }

    /* Open socket stream */
    client->file = fdopen(client->fd, "r+");
    if (client->file < 0) {
    	close(client->fd);
    	return -1;
    }

    return 0;
}

void *
handle_client(void *arg)
{
    struct echo_client *client = (struct echo_client *)arg;
    char buffer[BUFSIZ];

    fprintf(stderr, "%s:%s connected\n", client->host, client->port);

    /* Read from client, log to stdout, write to client */
    while (true) {
	/* Read from client */
	if (fgets(buffer, BUFSIZ, client->file) == NULL) {
	    break;
	}

	/* Log to stdout */
	printf("%s:%s says %s", client->host, client->port, buffer);

	/* Echo to client */
	if (fputs(buffer, client->file) == EOF) {
	    break;
	}
    }

    fprintf(stderr, "%s:%s disconnected\n", client->host, client->port);
    close(client->fd);

    pthread_exit(NULL);
}
