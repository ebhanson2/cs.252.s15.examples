#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <unistd.h>

void *
hello_thread(void *arg)
{
    int64_t tid = (int64_t)arg;
    
    printf("Hello, World From Thread %ld\n", tid);

    pthread_exit(NULL);
}

int
main(int argc, char *argv[])
{
    pthread_t *threads;
    int n = 10;

    /* Parse command line arguments */
    if (argc == 2) {
    	n = strtol(argv[1], NULL, 10);
    }

    /* Allocate threads array */
    threads = calloc(n, sizeof(pthread_t));
    if (threads == NULL) {
    	perror("calloc");
	return EXIT_FAILURE;
    }

    /* Launch threads */
    for (int64_t i = 0; i < n; i++) {
    	pthread_create(&threads[i], NULL, hello_thread, (void *)i);
    }

    /* Wait for threads */
    for (int i = 0; i < n; i++) {
    	pthread_join(threads[i], NULL);
    }

    return EXIT_SUCCESS;
}
