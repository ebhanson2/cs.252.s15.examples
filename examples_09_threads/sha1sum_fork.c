#include <openssl/sha.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    /* Ignore children */
    signal(SIGCHLD, SIG_IGN);

    /* Scan directory, filtering out directories */
    for (int i = 1; i < argc; i++) {
    	int      fd;
    	int      nread;
    	char	 buffer[BUFSIZ];
    	uint8_t  digest[SHA_DIGEST_LENGTH];
    	SHA_CTX  ctx;
    	pid_t    pid;

    	pid = fork();
    	switch (pid) {
	    case -1:	/* Error */
		perror("fork");
	    	break;
	    case  0:	/* Child */
	    	/* Open file for reading */
	    	fd = open(argv[i], O_RDONLY);
	    	if (fd < 0) {
	    	    perror("open");
	    	    return EXIT_FAILURE;
		}

		/* Read file and compute checksum */
		SHA1_Init(&ctx);
		while ((nread = read(fd, buffer, BUFSIZ)) > 0) {
		    SHA1_Update(&ctx, buffer, nread);
		}
		SHA1_Final(digest, &ctx);

		/* Print checksum */
		for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
		    printf("%02x", digest[i]);
		}
		printf("  %s\n", argv[i]);

		/* Close file */
		close(fd);
		return EXIT_SUCCESS;
	    default:	/* Parent */
	    	break;
	}

    }

    return EXIT_SUCCESS;
}
