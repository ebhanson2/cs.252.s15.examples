/* addition.c: http://en.wikipedia.org/wiki/Bitwise_operations_in_C#A_simple_addition_program */

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int carry;
    int sum;
    int x;
    int y;

    if (argc != 3) {
    	fprintf(stderr, "usage: %s x y\n", argv[0]);
    	return EXIT_FAILURE;
    }

    x = atoi(argv[1]);
    y = atoi(argv[2]);

    do {
    	sum   = x ^ y;
    	carry = x & y;
    	x     = sum;
    	y     = carry << 1;
    } while (carry != 0);

    printf("%s + %s = %d\n", argv[1], argv[2], sum);

    return EXIT_SUCCESS;
}
