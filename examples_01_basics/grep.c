/* grep.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[])
{
    char buffer[BUFSIZ];

    if (argc != 2) {
    	fprintf(stderr, "usage: %s needle\n", argv[0]);
    	return EXIT_FAILURE;
    }

    for (int i = 1; fgets(buffer, BUFSIZ, stdin) != NULL; i++) {
    	if (strstr(buffer, argv[1])) {
	    printf("%d: %s", i, buffer);
	}
    }

    return EXIT_SUCCESS;
}
