#include <fstream>
#include <iostream>
#include <iterator>
#include <string>

using namespace std;

int
main(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++) {
	ifstream file;
	string   line;

	file.open(argv[i]);
	if (!file.is_open()) {
	    cerr << "Cannot open " << argv[i] << endl;
	    return EXIT_FAILURE;
	}

	while (getline(file, line)) {
	    cout << line << endl;
	}
    }

    return EXIT_SUCCESS;
}
