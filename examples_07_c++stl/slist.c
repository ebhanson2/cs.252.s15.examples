/* singly-linked list: slist.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ITEMS   8

static char *FRUITS[] = {"Apples", "Bananas", "Oranges", NULL};

typedef int (*compare_func_t)(const void *, const void *);

/* 1. Define Struct */
struct slist {
    void	    *data;
    struct slist    *next;
};

/* 2. Define function to allocate and initialize struct */
struct slist *
slist_create(void *data, struct slist *next)
{
    struct slist *s;

    s = malloc(sizeof(struct slist));
    if (s == NULL) {
    	return NULL;
    }

    s->data = data;
    s->next = next;
    return s;
}

/* 3. Define function add new data to front of list */
struct slist *
slist_push_front(struct slist *head, void *data)
{
    return slist_create(data, head);
}

/* 4. Define function to search list for specified data */
struct slist *
slist_find(struct slist *head, void *data)
{
    for (struct slist *s = head; s; s = s->next) {
    	if (s->data == data) {
    	    return s;
	}
    }

    return NULL;
}

/* 5. Define function to print list assuming longs  */

void
slist_print_long(struct slist *head)
{
    for (struct slist *s = head; s; s = s->next) {
    	printf("%ld\n", (long)s->data);
    }
}

/* 6. Define function to search list for specified data using comparison function */

struct slist *
slist_search(struct slist *head, void *data, int(compare)(const void *, const void*))
{
    for (struct slist *s = head; s; s = s->next) {
    	if (compare(s->data, data) == 0) {
    	    return s;
	}
    }

    return NULL;
}

/* 7. Define function to compare two longs */

int
long_compare(const void *a, const void *b)
{
    const long ia = (const long)(a);
    const long ib = (const long)(b);

    return (int)(ia - ib);
}


/* 8. Define function to print list assuming strings */

void
slist_print_string(struct slist *head)
{
    for (struct slist *s = head; s; s = s->next) {
    	printf("%s\n", (char *)s->data);
    }
}

int
main(int argc, char *argv[])
{
    struct slist *s = NULL;
    struct slist *p = NULL;
    char buffer[BUFSIZ];

    /* Add numbers to list */
    for (long i = 0; i < MAX_ITEMS; i++) {
	s = slist_push_front(s, (void *)i);
    }

    /* Print list */
    slist_print_long(s);

    /* Search list using find */
    for (long i = 0; i < MAX_ITEMS + 1; i++) {
	p = slist_find(s, (void *)i);
	printf("Is %ld in s? %d\n", i, p != NULL);
    }
    
    /* Search list using search */
    for (long i = 0; i < MAX_ITEMS + 1; i++) {
	p = slist_search(s, (void *)i, long_compare);
	printf("Is %ld in s? %d\n", i, p != NULL);
    }

    /*------------------------------------------------------------------------*/

    s = NULL;

    /* Add strings to list */
    for (char **fruit = FRUITS; *fruit; fruit++) {
	s = slist_push_front(s, *fruit);
    }

    /* Print list */
    slist_print_string(s);

    strcpy(buffer, "Apples");

    /* Search list using find */
    p = slist_find(s, buffer);
    printf("Is %s in s? %d\n", buffer, p != NULL);
    
    /* Search list using search */
    p = slist_search(s, buffer, (compare_func_t)strcmp);
    printf("Is %s in s? %d\n", buffer, p != NULL);

    return EXIT_SUCCESS;
}
