/* graph.cpp: simple graph example ------------------------------------------ */

#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>

using namespace std;

/* Data Structures ---------------------------------------------------------- */

typedef set<string> Edges;
typedef map<string, Edges> Graph;

/* Functions ---------------------------------------------------------------- */

static void
read_graph(Graph &g)
{
    string line;

    while (getline(cin, line)) {
	stringstream ss(line);
	string	     node;
	string	     edge;

	ss >> node;
	g[node] = set<string>();

	while (ss >> edge) {
	    g[node].insert(edge);
	}
    }
}

static void
print_graph(Graph &g)
{
    for (auto node : g) {
	cout << node.first;
	for (auto edge : node.second) {
	    cout << " " << edge;
	}
	cout << endl;
    }
}

/* Main Execution ----------------------------------------------------------- */

int
main(int argc, char *argv[])
{
    Graph g;

    read_graph(g);
    print_graph(g);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 ft=cpp ------------------------------------------ */
